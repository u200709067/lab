public class GCDLoop {
    public static void main(String[] args) {
        if (args.length<2){

            System.out.println("Please provide two integers: ");
        }
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);

        int result = gcd(a > b ? a : b, a>b ? b : a);


        System.out.println("GCD of "+a+" and "+b+" = "+result);
    }

    public static int gcd (int a , int b){
        int remainder = a % b;
        do {
            remainder = a % b;
            a = b;
            b = remainder;
        }
        while (remainder != 0);
        return a;
    }
}
